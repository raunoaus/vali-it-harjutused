import java.util.ArrayList;
import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        // Keskseks teemaks on array ehk massiiv
        int[] massiiv = new int[6];
        ArrayList list = new ArrayList();

        // Prindi välja massiiv
        String massiivStr = Arrays.toString(massiiv);
        System.out.println(massiivStr);

        // Muuda kolmas number massiivis number 5´ks

        massiiv[2] = 5;
        System.out.println(Arrays.toString(massiiv));

        // Määra talle väärtus ja prindi välja kuues element massiivist
        massiiv[5] = 4;
        System.out.println(massiiv[5]);

        // Prindi välja viimane element, ükskõik kui pikk massiiv ka ei oleks

        int viimane = massiiv[massiiv.length-1];
        System.out.println("Viimane " + viimane);

        // Loo uus massiiv, kus on kõik 8 numbrit kohe alguses määratud

        int[] massiiv8 = new int[]{1,2,3,4,5,6,7,8};
        System.out.println(Arrays.toString(massiiv8));

        // Prindi välja ükshaaval kõik väärtused massiiv8-st

        int index = 0;
        while (index < massiiv8.length) {
            System.out.println(massiiv8[index]);   // tavaline tsükkel
            // suurene ühe võrra
            index++;
        }

        // Teeme selle sama tsükli kiiremini kasutades for tsüklit

        for (int i = 0; i < massiiv8.length; i++) {
            System.out.println(massiiv8[i]);             // indexiga tsükkel
        }

        // Loo stringide massiiv, mis on alguses tühi aga siis lisad ka keskele mingi sõne

        String[] stringMassiiv = new String[3];
        stringMassiiv[1] = "sõne";
        System.out.println(Arrays.toString(stringMassiiv));

        // 1. Loo massiiv, kus on sada kohta
        // 2. Täida massiiv järjest loeteluga alustades numbrist 0
        // 3. Prindi see massiiv välja

        int[] loetelu = new int [100];
        int number = 0;
        for (int i = 0; i <loetelu.length; i++) {
            loetelu[i] = number; // kirjutab positsiooni üle
            number++;
        }
            System.out.println(Arrays.toString(loetelu));

        // 1. kasuta loetelu massiivi, kus on numbrite jada
        // 2. loe mitu paarisarvu on
        // 3. prindi tulemus
        // kasutada nii tsüklit kui if lauset
        // kui jagad kaks arvu operaatoriga %, siis see tagastab jäägi
        // x % 2 tähendab, et 0 puhul on paarisarv ja 1 puhul ei ole

        // Algoritm:
        // 1. Võtan esimese numbri massiivist. See on 0.
        // 2. Kas number on paaris või mitte?
        // 3. Kui  on paaris, siis liidan ühe loendajale otsa.

        int mituPaari = 0;
        for (int i = 0; i <loetelu.length ; i++) {
            if (i % 2 == 0 ) {
                mituPaari++;
            }
        }
        System.out.println(mituPaari);

        // Loo ArrayList ja sisesta sinna kolm numbrit ja kaks Stringi.

        ArrayList nimekiri = new ArrayList();
        nimekiri.add("Siga");
        nimekiri.add("Kägu");
        nimekiri.add(55);
        nimekiri.add(31);
        nimekiri.add(23);
        System.out.println(nimekiri);

        // Küsi viimasest listist kolmas element ja prindi välja

        System.out.println(nimekiri.get(2));

        // Prindi kogu list välja

        System.out.println(nimekiri);

        // Prindi iga element ükshaaval välja. Vaja on while või for tsüklit

        for (int i = 0; i <nimekiri.size(); i++) {
            System.out.println(nimekiri.get(i));

        }


        // 1. Loo uus array list, kus on 543 numbrit. Numbrid peavad olema suvalised. Math.random() vahemikus 0-10.
        // 2. korruta iga number viiega
        // 3. salvesta see uus number samale positsioonile

        //  for (int i = 0; i < 100 ; i++) {
        //  double nr = Math.random() * 2;     // See on vaid vihje
        //     System.out.println(nr);

        ArrayList eritiPikk = new ArrayList();
        for (int i = 0; i < 543; i++) {
            int nr = (int) (Math.random() * 11); // võimalik castida või kasutada math.floor´i
            eritiPikk.add(nr); // lisab numbri, mille välja arvutasime
        }
        System.out.println("Algne massiiv" + eritiPikk);

        for (int i = 0; i < eritiPikk.size(); i++) {
            int nr = (int) eritiPikk.get(i); // saab lisada sama nimega muutuja kuna eelnevalt on see defineeritud bloki sees
            int muudetudNr = nr * 5;  //võib lisada ka eelmisele reale lõppu * 5, kuid selguse mõttes on parem lisada uuele reale
            eritiPikk.set(i, muudetudNr);

        }
        System.out.println("Muudetud massiiv" + eritiPikk);















    }
}
