console.log("hommik!")

var sisend = 7

// kui vähem kui 7, korruta kahega
// kui suurem kui 7, jaga kahega
// kui on täpselt 7, jäta samaks

if (sisend == 7) {
	tulemus = sisend
} else if (sisend < 7) {
	tulemus = sisend * 2
} else {
	tulemus = sisend / 2
}


console.log("Tulemus on: " + tulemus)

/*
kui sõned on võrdsed, siis prindi üks, kui erinevad, siis liida kokku ja prindi.
*/

var str1 = "banaan"
var str2 = "apelsin"

if (str1 == str2) {
	console.log(str1)
} else {
	console.log(str1 + " " + str2)
}

/*
Meil on linnade nimekiri. aga ilma sõnata "linn". Need palun lisada.
*/

var linnad = ["Pärnu", "Tartu", "Valga"] // Defineerime nimekirja
var uuedLinnad = [] // Defineerime nimekirja kuhu tulemused panna

while (linnad.length > 0) { // Kuni linnad on listis
	var linn = linnad.pop() // Võta välja viimane
	var uusLinn = linn + " linn" // ja lisa "linn" otsa
	uuedLinnad.push(uusLinn) // tulemus salvestada uude listi
}

console.log(uuedLinnad)

/*
Eralda poiste ja tüdrukute nimed
*/

var nimed = ["Margarita", "Mara", "Martin", "Kalev"]
var poisteNimed = []
var tydrukuteNimed = []
// "a" lõpuga on tüdruku nimi, lisa see tydrukuteNimed listi
// Kui on poisi nimi, lisa see poisteNimed listi
while (nimed.length > 0) {
var nimi = nimed.pop()
if (nimi.endsWith("a")) {
	tydrukuteNimed.push(nimi)
} else {
	poisteNimed.push(nimi)
}
}
console.log(poisteNimed, tydrukuteNimed)

// ctrl + klikk saad mitu kursorit
// ctrl + enter tekitad uue rea
// console.log("muutuja", muutuja) prindib

/* FUNKTSIOONID */

/*
Loo funktsioon, mis tagastab vastuse küsimusele, kas tegu on numbriga?
!isNan(4) // is Not a Number
*/

var kasOnNumber = function(number) {
if (!isNaN (number)) {
	return true
} 
    return false 
}
console.log( kasOnNumber(4))
console.log( kasOnNumber("mingi sõne"))
console.log( kasOnNumber(23536))
console.log( kasOnNumber(6.876))
console.log( kasOnNumber(null))
console.log( kasOnNumber([1, 4, 5, 6]))

/* Kirjuta funktsioon, mis võtab vastu kaks numbrit ja tagastab nende summa.
*/

var numbriteSumma = function(a, b) {
	return a + b
}
console.log(numbriteSumma(4, 5))
console.log(numbriteSumma(7, 87))

/*

*/

var inimesed = {
	"Kaarel": 34,
	"Margarita": 10,
	"Suksu": [5, 6, 7],
	"Rauno": {
		vanus: 29,
		sugu: true
	}
}
console.log(inimesed["Kaarel"])
console.log(inimesed.Kaarel)
console.log(inimesed.Rauno.vanus)
console.log(inimesed.Suksu[1])