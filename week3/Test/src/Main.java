public class Main {
    public static void main(String[] args) {
        System.out.println("Test");

        // Ülesanne 1
        System.out.println(piKorrutis);

        // Ülesanne 2
        int a = 5;
        int b = 5;
        System.out.println(toeVaartus(a, b));

        // Ülesanne 3



    }


    // Ülesanne 1:
    // Deklareeri muutuja, mis hoiaks endas Pi (https://en.wikipedia.org/wiki/Pi) arvulist väärtust vähemalt 11 kohta peale koma.
    // Korruta selle muutuja väärtus kahega ja prindi standardväljundisse.

    static double piVaartus = 3.14159265358;
    static double piKorrutis = piVaartus * 2;


    // Ülesanne 2:
    // Kirjuta meetod, mis tagastab boolean-tüüpi väärtuse ja mille sisendparameetriteks on kaks täisarvulist muutujat.
    // Meetod tagastab tõeväärtuse vastavalt sellele,
    // kas kaks sisendparametrit on omavahel võrdsed või mitte. Meetodi nime võid ise välja mõelda.


    public static boolean toeVaartus(int a, int b) {
        if (a == b) {
            return true;
        } else {
            return false;
        }
    }

    // Ülesanne 3:
    // Kirjuta meetod, mille sisendparameetriks on Stringide massiiv ja mis tagastab täisarvude massiivi.
    // Tagastatava massiivi iga element sisaldab endas vastava sisendparameetrina vastu võetud massiivi elemendi (stringi) pikkust.
    // Meetodi nime võid ise välja mõelda.
    // Kutsu see meetod main()-meetodist välja ja prindi tulemus (kõik massiivi elemendid) standardväljundisse.

    public static int[] stringiPikkus(String[] stringiMassiiv ) {
        int[] tagastus = new int[stringiMassiiv.length];
        for (int i = 0; i < stringiMassiiv.length; i++) {
            tagastus[i] = Integer.parseInt(stringiMassiiv[i]);
        }
            return tagastus;

    }

}
