package ee.valiit.kasutajatelist;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@CrossOrigin
@RestController


public class APIController {


    @Autowired
    private JdbcTemplate jdbcTemplate;

    @PostMapping ("/list")
    public void handleNewListItem(@RequestBody Kasutaja kasutaja ) {
        System.out.println("handleNewListItem");
        System.out.println("kasutaja nimi: " + kasutaja.getNimi());
        String sqlKask = "INSERT INTO kasutajad(nimi, vanus) VALUES ('"
                + kasutaja.getNimi() + "', " + kasutaja.getVanus() + ");";
        jdbcTemplate.execute(sqlKask);
        System.out.println("Sisestamine õnnestus");
    }

    @GetMapping("/list")
    public ArrayList<Kasutaja> printListItems() {
        System.out.println("printListItems käivitus");
        ArrayList<Kasutaja> nimekiri = new ArrayList<>();
        nimekiri =(ArrayList) jdbcTemplate.queryForList("SELECT * FROM kasutajad");
        return nimekiri;

    }


}
