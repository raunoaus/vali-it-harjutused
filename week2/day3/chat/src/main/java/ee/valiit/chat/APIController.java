package ee.valiit.chat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.*;

import javax.naming.ConfigurationException;
import java.util.ArrayList;

@RestController
@CrossOrigin
public class APIController {

    @Autowired // käsk: ühenda klass
            JdbcTemplate jdbcTemplate;


    @GetMapping("/chat/{room}")
        //SELECT * FROM messages
    ArrayList<ChatMessage> chat(@PathVariable String room) {
        try {
        String sqlKask = "SELECT * FROM messages WHERE room='"+room+"'";
        ArrayList<ChatMessage> messages = (ArrayList) jdbcTemplate.query(sqlKask, (resultSet, rowNum) -> {
            String username = resultSet.getString("username");
            String message = resultSet.getString("message");
            String avatar = resultSet.getString("avatar");
            return new ChatMessage(username, message, avatar);

        });
        return messages;
    } catch (DataAccessException err) { // kui tekib error, siis proovi midagi muud
            System.out.println("TABLE WAS NOT READY");
            return new ArrayList();
        }


    }

    @PostMapping("/chat/{room}/new-message")
    void newMessage(@RequestBody ChatMessage msg, @PathVariable String room) {
        String sqlKask = "INSERT INTO messages (username, message, avatar, room) VALUES ('" + msg.getUsername() + "', '" + msg.getMessage() + "', '" + msg.getAvatar() + "', '" + room + "')";
        jdbcTemplate.execute(sqlKask);


    }

}


