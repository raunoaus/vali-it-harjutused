package ee.valiit.chat;

public class ChatMessage {
    private int id;
    private String username;
    private String room;
    private String message;
    private String avatar;

    public ChatMessage() {

    }

    public ChatMessage(String username, String message, String avatar) {
        this.username = username;
        this.message = message;
        this.avatar = avatar;
    }


    public String getUsername() {
        return Security.xssFix(username);
    }

    public String getRoom() {
        return room;
    }

    public String getMessage() {
        return Security.xssFix(message);
    }

    public int getId() {
        return id;
    }

    public String getAvatar() {
        return avatar;
    }
}
