console.log("Töötan!")

// 1. Ül: Alla laadida API´st tekst.

var refreshMessages = async function() {
	// Selleks, et teada, et funktsioon läks käima
	console.log("refreshMessages läks käima")
	// API aadress on string, salvestan muutujasse
	// Uus muutuja kuhu salvestame praeguse toa
	var tuba = document.querySelector("#room").value
	var APIurl = "http://localhost:8080/chat/" + tuba
	// fetch teeb päringu serverisse (meie defineeritud aadress)
	var request = await fetch(APIurl)
	// json() käsk vormindab andmed jsoniks
	var sonumid = await request.json()
	console.log(sonumid)

	// document.querySelector('#jutt').innerHTML = JSON.stringify(json)

	// Kuva serverist saadud info HTMLis ehk lehel
	document.querySelector('#jutt').innerHTML = ""
	// var sonumid = json.messages //lihtsustamiseks
	while (sonumid.length > 0) { // kuniks sõnumeid on
		var sonum = sonumid.shift()
		console.log(sonum)
		//Lisa HTMLi #jutt sisse sonum.message
document.querySelector('#jutt').innerHTML +=`<p><img src='${sonum.avatar}' width="100">${sonum.username}: ${sonum.message}</p>`


// Scrolli kõige alla
window.scrollTo(0,document.body.scrollHeight);

	}
}
setInterval(refreshMessages, 1000) // 1000 on yks sekund


document.querySelector('form').onsubmit = function(event) {
	event.preventDefault()
	console.log("submit käivitus")
	// Korjame kokku formist info
	var username = document.querySelector('#username').value
	var message = document.querySelector('#message').value
	var avatar = document.querySelector('#avatar').value
	console.log(username, message)
// POST päring postitab uue andmetüki serverisse
    var tuba = document.querySelector("#room").value
    var APIurl = "http://localhost:8080/chat/"+tuba+"/new-message" // see on serveri poolt antud URL
	fetch(APIurl, {
		method: "POST",
		body: JSON.stringify({username: username, message: message, avatar: avatar }), ///
		headers: {
			'Accept': 'application/json' ,
			'Content-Type': 'application/json'
		}
	})
}
