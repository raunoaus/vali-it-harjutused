package ee.valiit.chat;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class ChatMessage {

    private String user;
    private String message;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm") //jsoni kuupäeva formattimiseks
    private Date date;
    private String url;

    public ChatMessage(){

    }

    public ChatMessage(String user, String message, String url) {
        this.user = user;
        this.message = message;  //yhendamaks userid ja messaged
        this.url = url;
        this.date = new Date();



    }

    public String getUser() {
        return user;
    }

    public String getMessage() {
        return message;
    }

    public Date getDate() {
        return date;
    }

    public String getUrl() {
        return url;
    }
}


