package ee.valiit.chat;

import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

/*
    Ülesanne:
    1. Peale useri ja message lisa ka profiili pildi URL, mis salvestub serverisse ja
    ja kuvatakse HTML-is välja. Kasutaja saab kolmanda välja kuhu see URL panna.
    2. Loo uus chatroom lisaks generalile.
 */

@RestController
@CrossOrigin
public class APIController {
    HashMap<String, Chatroom> rooms = new HashMap();

    public APIController() {
        rooms.put("general", new Chatroom("general"));
        rooms.put("random", new Chatroom("random"));
        rooms.put("materjalid", new Chatroom("materjalid"));
    }


    @GetMapping("/chat/{room}")
    Chatroom chat(@PathVariable String room) {  //
        return rooms.get(room);

    }

    @PostMapping("/chat/general/new-message")
    void newMessage(@RequestBody ChatMessage msg, @PathVariable String room) {
        rooms.get(room).addMessage(msg);  ///

    }






}
