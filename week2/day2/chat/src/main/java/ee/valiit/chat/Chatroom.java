package ee.valiit.chat;

import java.util.ArrayList;

public class Chatroom {
    public String room;
    public ArrayList<ChatMessage> messages = new ArrayList();
    public Chatroom(String room) {
        this.room = room; //this viitab klassile
        messages.add(new ChatMessage("Rauno", "Hei!", "URL"));

    }

    public void addMessage(ChatMessage msg) {
        messages.add(msg);
    }
}
