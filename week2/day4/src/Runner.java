public class Runner extends Sportlane {
    public Runner(String perenimi) {
        super(perenimi);
    }

    @Override
    public void perform() {
        System.out.println(perenimi + ": jookseb");
    }
}
