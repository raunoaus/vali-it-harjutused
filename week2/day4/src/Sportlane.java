public abstract class Sportlane {
    String eesnimi;
    String perenimi;
    int vanus;
    String sugu;
    double pikkus;
    double kaal;

    public abstract void perform(); // abstract tagab, et kõikidel sportlastel on perform meetod

    public Sportlane(String perenimi) {
        this.perenimi = perenimi;
    }



}
