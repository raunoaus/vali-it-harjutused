public class Suusataja {
    int stardiNr;
    double kiirus;
    double labitudDistants;
    double dopinguKasutaja;

    public Suusataja(int i) {
        stardiNr = i;
        kiirus = 1 + Math.random()* 20;//20 km/h
        labitudDistants = 0;
        dopinguKasutaja = Math.random() * 15;
    }



    public void suusata() {
        labitudDistants += kiirus / 3600;

    }

    public String toString() {
        int dist = (int)(labitudDistants * 1000);
        return stardiNr +": " + dist;
    }

    public boolean kasOnLopetanud(int koguDistants) {
        return labitudDistants >= koguDistants;
    }
}


