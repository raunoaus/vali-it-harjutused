import java.util.ArrayList;

public class Võistlus {
    ArrayList<Suusataja>voistlejad;
    int koguDistants;


    public Võistlus() {
        System.out.println("Start");
        voistlejad = new ArrayList();
        koguDistants = 20;
        for (int i = 0; i < 50 ; i++) {
            voistlejad.add(new Suusataja(i));


        }

        aeg();

    }

    public void aeg() {
        for(Suusataja s: voistlejad) {
            s.suusata();
            boolean lopetanud = s.kasOnLopetanud(koguDistants);
            if (lopetanud) {
                System.out.println("Võitja on: " + s);
                return;
            }

        }

        System.out.println(voistlejad);

        try {
            Thread.sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();

        }
        aeg();
    }
}
