import java.sql.SQLOutput;
import java.util.HashMap;

public class Main {

    public static void main(String[] args) {
        System.out.println("Tere!");

        // 1.Loo kolm muutujat numbritega
        // 2.Pane need muutujad lause sisse
        // 3.Prindi see lause välja

        int aasta = 89;
        int kuu = 10;
        int paev = 16;
        String lause = "Ma sündisin aastal " + aasta + ". Kuu oli "+kuu+" ja kuupäev "+paev+".";
        System.out.println(lause);

        // %d on täisarv
        // %f on komakohaga
        // %S on string

        String parem = String.format("Mina sündisin aastal %d. Kuu oli %d ja kuupäev %d.", aasta, kuu, paev);
        System.out.println(parem);

        //Loe kokku mitu akent on klassis ja pane see info hashmappi
        //Loe kokku mitu lampi on klassis ja pane see info hashmappi
        //Loe kokku mitu inimest on klassis ja pane see info hashmappi

        HashMap klassiAsjad = new HashMap();
        klassiAsjad.put("aknad", 5);
        klassiAsjad.put("lambid", 11);
        klassiAsjad.put("inimesed", 21);
        klassiAsjad.put(6,"tasapinnad");
        System.out.println(klassiAsjad);

        // Mõtle välja kolm praktilist kasutust hashmapile
        // 1. Isikuandmed
        // 2.

        // Prindi välja kui palju on inimesi klassis kasutades juba loodud hashmapi


        System.out.println(klassiAsjad.get("inimesed"));

        // Lisa samasse hashmapi juurde mitu tasapinda on klassis. enne number, siis string

        System.out.println(klassiAsjad);

        // Loo uus hashmap, kuhu saab sisestada ainult string:double paare. Sisesta sinna midagi

        HashMap<String, Double> uusMap = new HashMap<>();
        uusMap.put("pikkus", 1.8);
        uusMap.put("laius", 3.4);
        uusMap.put("kõrgus", 2.2);
        System.out.println(uusMap);

        // switch

        int rongiNr = 50;
        String suund = null;
        switch (rongiNr) {
            case 50:
                suund = "Pärnu"; break;
            case 55:
                suund = "Haapsalu"; break;
            case 10:
                suund = "Vormsi"; break;
        }

                System.out.println(suund);

        // FOREACH

        int[] mingidNumbrid = new int[] {8, 4, 5, 345, 4435, 6732356};

        for (int i = 0; i < mingidNumbrid.length; i++) {
            System.out.println(mingidNumbrid[i]);


        }
        System.out.println("-------------------------------");

        for (int nr : mingidNumbrid) {
            System.out.println(nr);
        }

        // õpilane saab töös punkte nullist sajani
        // kui punkte on alla 50, kukub ta läbi
        // vastasel juhul on hinne punktid/20
        // 100 punkti => 5
        // 80 punkti => 4
        // 67 punkti => 3
        // 50 punkti => 2

        int punkte = 134;
        if (punkte > 100 || punkte < 0) {
            throw new Error();
        }
        switch ((int) Math.round(punkte / 20.0)) {
            case 5:
                System.out.println("suurepärane"); break;
            case 4:
                System.out.println("hea"); break;
            case 3:
                System.out.println("rahuldav"); break;
            case 2:
                System.out.println("nigel"); break;
            default:
                System.out.println("läbikukkumine");
        }



        }




}
