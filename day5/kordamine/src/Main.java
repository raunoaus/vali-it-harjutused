import Pyhapaev.Pyhapaev;
import nadalapaevad.Laupäev;
import nadalapaevad.Reede;

public class Main {

    public static void main(String[] args) {
        System.out.println("Hello World!");

        if ((false && true) || true) { // boolean. kas true või false
            System.out.println("tõene");

        } else {
            System.out.println("väär");
        }

        Reede.koju(); // reede on klass, koju on meetod. shortcut alt + enter

        Laupäev.peole();


        Pyhapaev.hommik();


        Pyhapaev paev = new Pyhapaev();
        paev.maga();
        paev.hommik();
        // paev.uni(); punane kuna private

}}
